import math
import matplotlib.pyplot as plt
import numpy

data = [0.1000, 0.1455, 0.1909, 0.2364, 0.2818, 0.3273, 0.3727, 0.4182, 0.4636, 0.5091, 0.5545, 0.6000, 0.6455, 0.6909,
        0.7364, 0.7818, 0.8273, 0.8727, 0.9182, 0.9636]

LEARNING_STEP = 0.2


def get_desired_result(x):
    return (1 + 0.6 * math.sin(2 * math.pi * x / 0.7) + 0.3 * math.sin(2 * math.pi * x)) / 2


def activate(x):
    return 1/(1+math.exp(-1 * x))


def get_desired_results_in_array(x_array):
    result = []
    for x in x_array:
        result.append(get_desired_result(x))

    return result


def get_final_output(y2, y3, y4, y5, w, b):
    return y2 * w[4] + y3 * w[5] + y4 * w[6] + y5 * w[7] + b[4]


def get_actual_results_in_array(w, b, x_array):
    result = []
    for x in x_array:
        y2 = activate(x * w[0] + b[0])
        y3 = activate(x * w[1] + b[1])
        y4 = activate(x * w[2] + b[2])
        y5 = activate(x * w[3] + b[3])
        result.append(get_final_output(y2, y3, y4, y5, w, b))
    return result


def get_random_float():
    rnd = numpy.random.randn(1)[0]
    return round(rnd, 2)


def get_initial_values(integer):
    values = dict()
    for i in range(integer):
        values[i] = get_random_float()
    return values


def get_neuron_output(x, w, b):
    return activate(x * w + b)


def calculate():
    w = get_initial_values(8)
    b = get_initial_values(5)
    iterations = 1

    while iterations < 50000:
        for x in data:
            y2 = get_neuron_output(x, w[0], b[0])
            y3 = get_neuron_output(x, w[1], b[1])
            y4 = get_neuron_output(x, w[2], b[2])
            y5 = get_neuron_output(x, w[3], b[3])
            o = get_final_output(y2, y3, y4, y5, w, b)

            desired = get_desired_result(x)
            fault = desired - o

            w[7] = w[7] + LEARNING_STEP * y5 * 1 * fault
            w[6] = w[6] + LEARNING_STEP * y4 * 1 * fault
            w[5] = w[5] + LEARNING_STEP * y3 * 1 * fault
            w[4] = w[4] + LEARNING_STEP * y2 * 1 * fault

            b[4] = b[4] + LEARNING_STEP * 1 * fault * 1

            w[3] = w[3] + (LEARNING_STEP * y5*(1-y5)*w[7]*fault * x)
            w[2] = w[2] + (LEARNING_STEP * y4*(1-y4)*w[6]*fault * x)
            w[1] = w[1] + (LEARNING_STEP * y3*(1-y3)*w[5]*fault * x)
            w[0] = w[0] + (LEARNING_STEP * y2 * (1 - y2) * w[4] * fault * x)

            delta5 = LEARNING_STEP * y5 * (1 - y5) * fault * w[7] * 1
            delta4 = LEARNING_STEP * y4 * (1 - y4) * fault * w[6] * 1
            delta3 = LEARNING_STEP * y3 * (1 - y3) * fault * w[5] * 1
            delta2 = LEARNING_STEP * y2 * (1 - y2) * fault * w[4] * 1

            b[3] = b[3] + delta5
            b[2] = b[2] + delta4
            b[1] = b[1] + delta3
            b[0] = b[0] + delta2

        iterations = iterations + 1
        #
        # plt.cla()
        # plt.plot(data, get_desired_results_in_array(data))
        # plt.scatter(data, get_actual_results_in_array(w, b, data), marker='o', color='red')
        # plt.title('Iterations ' + str(iterations))
        # plt.pause(0.05)

    return w, b


weights, bases = calculate()
plt.plot(data, get_desired_results_in_array(data))
plt.scatter(data, get_actual_results_in_array(weights, bases, data), marker='o', color='red')
plt.show()


